#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo
rm -rf /home/ubuntu/finesse/

# clone the repo again
git clone git@gitlab.com:umakanth158/finesse.git

#source the nvm file. In an non
#If you are not using nvm, add the actual path like
# PATH=/home/ubuntu/node/bin:$PATH
#source /home/ubuntu/.nvm/nvm.sh

# stop the previous pm2
#pm2 kill
#npm remove pm2 -g


#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.
#npm install pm2 -g
# starting pm2 daemon
#
cd /home/ubuntu/finesse
echo ${PWD} "is the current Dir"

sudo chmod 777 ./deploy/docker.sh
sudo ./deploy/docker.sh

CONTAINER_LIST=$(sudo docker container list -q)

echo running_If

sudo docker stop $CONTAINER_LIST || true && sudo docker rm $CONTAINER_LIST || true

#if [ echo ${#CONTAINER_LIST[@]} -ge 1 ]; then
#  sudo docker container stop $CONTAINER_LIST];
#  sleep 20
#  echo stopping
#fi
sudo docker build -t udarapaneni/finesse_v01 .

sudo docker run -d -p 443:443 udarapaneni/finesse_v01


#install npm packages

#npm install

#Restart the node server
#npm start
echo "Running Diagnostics"
echo "RAM data after deoployment: $(free -h)"
echo "File System space details after deoployment: $(df -mhH /dev/root)"
echo "Good to Go!"
