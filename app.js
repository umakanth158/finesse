require('dotenv').config();

const mongoose = require('mongoose');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const https = require('https');
const path = require('path');
const fs = require('fs');

//My routes
const artRoutes = require("./src/routes/arts");
const categoryRoutes = require("./src/routes/category");
const sellerRoutes = require("./src/routes/seller");
const authRoutes = require("./src/routes/auth");
const artistRoutes = require("./src/routes/artist");

//DB Connection
console.log('test log');

mongoose
  .connect(process.env.DATABASE
  , {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  }
);

const db = mongoose.connection;
db.once('open', console.log.bind(console, 'DB connected 😊'));
db.on('error', console.error.bind(console, 'DB connection error'));

//Middlewares
app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors());

//My Routes
app.use("/api", authRoutes);
app.use("/api", artRoutes);
app.use("/api", categoryRoutes);
app.use("/api", sellerRoutes);
app.use("/api", artistRoutes);


//SSL Server
//PORT
const port = process.env.PORT || 8000;
const sslServer = https.createServer(
  {
    key: fs.readFileSync(path.join(__dirname, 'cert', 'private.key')),
    cert: fs.readFileSync(path.join(__dirname, 'cert', 'certificate.crt')),
  },
  app
).listen(port, () => {
  console.log(`app is running at ${port}`);
});


