const Artist = require("../models/artist");
const _ = require("lodash")

exports.getArtistById = (req, res, next, id) => {
    Artist.findById(id)
    .exec((err, artist) => {
        if (err) {
            return res.status(400).json({
                error: "Artist not found"
            });
        }
        req.artist = artist;
        next();
    });
}

exports.createArtist = (req,res) => {
    const artist = new Artist(req.body);
    artist.save((err, artist) => {
        if(err) {
            return res.send("Failed during creating Artist" + err)
        }
        return res.send(artist)
    })
}

exports.updateArtist = (req, res) => {
    let artist = req.artist;
    artist = _.extend(artist, req.body);
    artist.save((err, artist) => {
        if(err) {
            return res.send("update failed ")
        }
        return res.send(artist)
    })
}

exports.getAll=  (req, res) => {
    Artist.find().exec((err, Artists) => {
      if (err) {
        return res.status(400).json({
          error: "No Artists found"
        });
      }
      res.json(Artists);
    });
  };

  exports.deleteArtist = (req, res) => {
    const artist = req.artist;
    artist.remove((err, artist) => {
        if (err) {
          return res.status(400).json({
            error: "Failed to delete this artist"
          });
        }
        res.json({
          message: "Successfull deleted"
        });
      });
  }