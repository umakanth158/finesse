const Seller = require("../models/seller");
const _ = require("lodash")

exports.getSellerById = (req, res, next, id) => {
    Seller.findById(id)
    .exec((err, seller) => {
        if (err) {
            return res.status(400).json({
                error: "Seller not found"
            });
        }
        req.seller = seller;
        next();
    });
}

exports.createSeller = (req,res) => {
    const seller = new Seller(req.body);
    seller.save((err, seller) => {
        if(err) {
            return res.send("Failed during creating Seller" + err)
        }
        return res.send(seller)
    })
}

exports.updateSeller = (req, res) => {
    let seller = req.seller;
    seller = _.extend(seller, req.body);
    seller.save((err, seller) => {
        if(err) {
            return res.send("update failed ")
        }
        return res.send(seller)
    })
}

exports.getAll=  (req, res) => {
    Seller.find().exec((err, Sellers) => {
      if (err) {
        return res.status(400).json({
          error: "No Sellers found"
        });
      }
      res.json(Sellers);
    });
  };

  exports.deleteSeller = (req, res) => {
    const seller = req.seller;
    seller.remove((err, seller) => {
        if (err) {
          return res.status(400).json({
            error: "Failed to delete this seller"
          });
        }
        res.json({
          message: "Successfull deleted"
        });
      });
  }