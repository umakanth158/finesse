const Arts = require('../models/art');
const formidable = require("formidable")
const fs = require('fs');
const awsService = require('../util/aws')
const { v4: uuidv4 } = require('uuid');
const { title } = require('process');
const _ = require("lodash")

// Load the SDK for JavaScript


exports.getArtById = (req, res, next, id) => {
    Arts.findById(id)
        .exec((err, art) => {
            if (err) {
                return res.status(400).json({
                    error: "Arts not found"
                });
            }
            req.art = art;
            next();
        });
};

exports.createArt = (req, res) => {
    var form = new formidable.IncomingForm();
    form.multiples = true;
    form.parse(req, (err, fields, files) => {
        if(err) {
            return res.send("failed while parsing form")
        }
     // validation of fields 
        const {title, description, price, category, seller, artist} = fields;
        if(!title || !description || !price || !category || !seller || !artist) {
            return res.status(400).json({error: 'Bad payload'})
        }
        let art = new Arts(fields);
        //handle file here
        if (files.photo && files.hasOwnProperty('photo')) {
            updateImages(req,res, art, files);
        } else {
            return res.status(400).send("bad request")
        }
    })
}

exports.updateArt = (req, res) => {
    let art = req.art;
    art = _.extend(art, req.body);
    art.save((err, art) => {
        if(err) {
            return res.send("update failed ");
             
        }
         res.send(art);
         return
    })
}

exports.updateNewProductImages = (req,res) => {
    var form = new formidable.IncomingForm();
    form.multiples = true;
    let art = req.art;
    form.parse(req, (err, fields, files) => {
        if(err) {
            return res.send("failed while parsing form")
        }
        if (files.photo && files.hasOwnProperty('photo')) {
            updateImages(req,res, art, files);
        } else {
            return res.status(400).send("bad request")
        }
    })
}

exports.deleteProductImages = (req,res) => {
    let art = req.art;
    let deleteList = req.body.data;
    console.log(deleteList)
    let promises = [];
    for( let i=0; i <deleteList.length; i++) {
        promises.push(awsService.deletePhotoFromS3Bucket(deleteList[i], 'bestbasket'))
    }
    return Promise.all(promises)
            .then( success => {
                let updated = art.photo;
                console.log(updated)
                for(let item of deleteList) {
                    let index  = updated.indexOf(item);
                    updated.splice(index, 1);
                };
                art.photo  = updated;
                art.save((err, art) => {
                    if (art) {
                        return res.json({art})
                    }
        
                    res.send('error')
                })
            })
            .catch( err => {
                console.log(err)
                return res.status(500).send({
                    error: err,
                    message: 'failed to delete photo, try again'
                })
    })  
}


exports.getAll = (req, res) => {
    Arts.find()
        .exec((err, products) => {
            if (products) {
                return res.status(200).json(products)
            }
            res.status(500).send(err)
        })
}


exports.deleteArt = (req, res) => {
    const art = req.art;
    art.remove((err, art) => {
        if (err) {
          return res.status(400).json({
            error: "Failed to delete this art"
          });
        }
        res.json({
          message: "Successfull deleted"
        });
      });
}


function updateImages(req,res, art, files) {
            let pics = [];
            if(Array.isArray(files.photo)) {
                pics = files.photo
            } else {
                pics = [{...files.photo}];
            }
            if(pics.some( item => item.size > 3000000)) {
                return res.status(400).json({
                    error: "File size too big!"
                });
            }
            let formattedPics =  pics.map(item => {
                let obj = {};
                obj.data = fs.readFileSync(item.path);
                obj.contentType = files.photo.type;
                return obj
            });
            let promises = []; let keys = [];
            for (let item of formattedPics) {
                 const key  = `${art.seller}/${uuidv4()}.jpg`;
                 keys.push(key);
                promises.push(awsService.savePhotoToS3Bucket(item,key,"bestbasket"))
            }
           return Promise.all(promises)
            .then( success => {
                art.photo = keys;
                art.save((err, art) => {
                    if (art) {
                        return res.json({art})
                    }
        
                    res.send('error')
                })
            })
            .catch( err => {
                return res.status(500).send({
                    error: err,
                    message: 'failed to save photo, try again'
                })
            })         
} 