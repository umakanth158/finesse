const express =require("express")
const router = express.Router();

const {createArtist, getArtistById, updateArtist, getAll,deleteArtist} = require("../controllers/artist")
const { isUserSignedIn, isSessionValid, isUserAdminOrSeller, getUserById } = require("../controllers/auth");

router.param('artistId', getArtistById)
router.param("userId", getUserById);

router.post('/artist/create/:userId', isUserSignedIn, isSessionValid,isUserAdminOrSeller,createArtist);
router.put('/artist/update/:artistId/:userId', isUserSignedIn, isSessionValid,isUserAdminOrSeller, updateArtist);
router.get('/artists/:userId', getAll);
router.delete("/artist/delete/:artistId/:userId",isUserSignedIn, isSessionValid,isUserAdminOrSeller, deleteArtist);

module.exports = router;