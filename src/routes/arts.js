const express = require("express");
const router = express.Router();
const { isUserSignedIn, isSessionValid, isUserAdmin, isUserAdminOrSeller,getUserById } = require("../controllers/auth");

const {createArt, deleteArt, getAll, getArtById, updateArt, updateNewProductImages, deleteProductImages} = require('../controllers/arts');
// params
router.param('artId', getArtById);
router.param("userId", getUserById);


router.post('/art/create/:userId',isUserSignedIn, isSessionValid,isUserAdminOrSeller,createArt);
router.put('/art/update/:artId/:userId', isUserSignedIn, isSessionValid,isUserAdminOrSeller,updateArt);
router.put('/art/update/images/:artId/:userId', isUserSignedIn, isSessionValid,isUserAdminOrSeller,updateNewProductImages);
router.delete('/art/delete/images/:artId/:userId', isUserSignedIn, isSessionValid,isUserAdminOrSeller,deleteProductImages);

router.get('/arts/:userId',isUserSignedIn, isSessionValid,isUserAdminOrSeller,getAll)

router.delete('/art/delete/:artId/:userId',isUserSignedIn, isSessionValid,isUserAdminOrSeller,deleteArt)

module.exports = router;