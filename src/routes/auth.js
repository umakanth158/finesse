const express =require("express");
const { body } = require("express-validator");
const router = express.Router();

const {signupUser, getUserById, updateUser, getAll,deleteUser, signin, signout} = require("../controllers/auth")

router.param('userId', getUserById);

router.delete("/user/delete/:userId",getUserById, deleteUser);


router.post('/user/signup',body('email').isEmail(), body('password').isLength({min: 5, max: 32}),body('firstname').isLength({min:5}),signupUser);
router.post( '/signin', body('email').isEmail(), body('password').isLength({min: 5, max: 32}),signin);
router.post( '/signout', signout);

module.exports = router;