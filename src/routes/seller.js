const express =require("express");
const router = express.Router();
const { isUserSignedIn, isSessionValid, isUserAdmin, getUserById } = require("../controllers/auth");
const {createSeller, getSellerById, updateSeller, getAll,deleteSeller} = require("../controllers/seller")

router.param('sellerId', getSellerById)
router.param("userId", getUserById);

router.post('/seller/create/:userId', isUserSignedIn, isSessionValid,isUserAdmin,createSeller);
router.put('/seller/update/:sellerId/:userId', isUserSignedIn, isSessionValid,isUserAdmin, updateSeller);
router.get('/sellers', getAll);
router.delete("/seller/delete/:sellerId/:userId",isUserSignedIn, isSessionValid,isUserAdmin, deleteSeller);

module.exports = router;