const express = require("express");
const { isUserSignedIn, isSessionValid, isUserAdmin, getUserById } = require("../controllers/auth");
const router = express.Router();
const cors = require("cors")
const {
  getCategoryById,
  createCategory,
  getCategory,
  getAllCategory,
  updateCategory,
  removeCategory
} = require("../controllers/category");

//params
router.param("categoryId", getCategoryById);
router.param("userId", getUserById);
//actual routers goes here

//create
router.post("/category/create/:userId",isUserSignedIn, isSessionValid,isUserAdmin,createCategory
);

//read
router.get("/category/:categoryId/:userId", getCategory);
router.get("/categories/:userId", cors(), isUserSignedIn, isSessionValid,isUserAdmin, getAllCategory);

//update
router.put("/category/:categoryId/:userId", isUserSignedIn, isSessionValid,isUserAdmin,updateCategory);

//delete

router.delete("/category/:categoryId/:userId",isUserSignedIn, isSessionValid,isUserAdmin,removeCategory);

module.exports = router;