const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const artistSchema = new mongoose.Schema({
  firstname: {
    type: String,
    trim: true,
    required: true,
    maxlength: 32,
  },
  lastName: {
    type: String,
    trim: true,
    required: false,
    maxlength: 32,
  },
  mobile: {
    type: Number,
    trim: true,
    maxlength: 15,
    unique: true,
  },
  email: {
    type: String,
    trim: true,
    maxlength: 32,
    unique: true,
    required: true,
  },
  about: {
    type: String,
    trim: true,
    required: false,
    maxlength: 2000,
  },
  artistPhoto: {
    type: String,
    trim: true,
  },
});

module.exports = mongoose.model("Artist", artistSchema);
