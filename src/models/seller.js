const mongoose = require("mongoose");

const sellerSchema = new mongoose.Schema(
  {
    firstName: {
      type: String,
      trim: true,
      required: true,
      maxlength: 32,
    },
    lastName: {
      type: String,
      trim: true,
      required: true,
      maxlength: 32,
    },
    email: {
      type: String,
      trim: true,
      required: true,
      maxlength: 32,
      unique: true,
    },
    mobileNumber: {
      type: String,
      required: false,
      maxlength: 15,
      trim: true,
      unique: true,
    },
    gstNumber: {
      type: String,
      unique: true,
    },
    address: {
      type: String,
      maxlength: 2000,
    },
    pincode: {
      type: String,
      maxlength: 10,
      trim: true,
      required: true,
    },
    state: {
      type: String,
      maxlength: 20,
      trim: true,
      required: true,
    },
    city: {
      type: String,
      maxlength: 20,
      trim: true,
      required: true,
    },
    country: {
      type: String,
      maxlength: 10,
      trim: true,
      required: true,
    },
    logo: {
      type: String,
      maxlength: 255,
      trim: true,
      required: false,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Seller", sellerSchema);
