const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const artsSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      trim: true,
      required: true,
      maxlength: 32,
    },
    description: {
      type: String,
      trim: true,
      required: true,
      maxlength: 2000,
    },
    artist: {
      type: ObjectId,
      ref: "Artist",
      required: true,
    },
    artInfo: {
      dimensions: {
        type: String,
        required: false,
        trim: true,
      },
      dimensionUnit: {
        type: String,
        required: false,
        trim: true,
      },
      material: {
        type: String,
        trim: true,
        required: false,
      },
      isColor: {
        type: Boolean,
        required: false,
      },
    },
    price: {
      type: Number,
      required: true,
      maxlength: 32,
      trim: true,
    },
    category: {
      type: String,
      required: true,
    },
    stock: {
      type: Number,
      default: 1,
    },
    sold: {
      type: Number,
      default: 0,
    },
    tags: {
      type: Array,
      required: false,
      maxlength: 20,
    },
    discount: {
      value: {
        type: String,
        required: false,
        trim: true,
      },
      unit: {
        type: String,
        required: false,
        trim: true,
      },
    },
    seller: {
      type: ObjectId,
      ref: "Seller",
      required: true,
    },
    active: {
      type: Boolean,
      require: false,
      trim: true,
    },
    photo: {
      type: Array,
      required: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Arts", artsSchema);
