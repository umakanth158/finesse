const AWS = require('aws-sdk');


exports.savePhotoToS3Bucket = function(pic, key, bucket) {
    AWS.config.update({region: 'us-east-2'});
    const S3 = new AWS.S3({credentials: { accessKeyId: process.env.AWS_accessKeyId, secretAccessKey: process.env.AWS_secretAccessKey }});

    const payload = {
        Body: pic.data,
        Key: key,
        Bucket: bucket
    }
    return S3.upload(payload).promise();
}

exports.deletePhotoFromS3Bucket = function( key, bucket) {
    AWS.config.update({region: 'us-east-2'});
    const S3 = new AWS.S3({credentials: { accessKeyId: process.env.AWS_accessKeyId, secretAccessKey: process.env.AWS_secretAccessKey }});

    const payload = {
        Key: key,
        Bucket: bucket
    }
    return S3.deleteObject(payload).promise();
}

exports.senEmail = function () {
    AWS.config.update({region: 'us-east-2'});
    const ses = new AWS.SES({credentials: { accessKeyId: process.env.AWS_accessKeyId, secretAccessKey: process.env.AWS_secretAccessKey }});

    let params = {
        Destination: {
            ToAddresses: [
                "saideep_konduri@outlook.com"
            ]
        },
        Message: {
            Body: {
                Html: {
                    Charset: "UTF-8",
                    Data: "<p>this is test body.</p>"
                },
                Text: {
                    Charset: "UTF-8",
                    Data: 'Hey, this is test.'
                }
            },
            
            Subject: {
                Charset: 'UTF-8',
                Data: "test"
            }
        },
        Source: 'kondurisaideep@gmail.com', 
        ReplyToAddresses: [
            'kondurisaideep@gmail.com',
        ],
    };
    // this sends the email
    return ses.sendEmail(params, (err, data) => {
    if (err) console.log(err)
    else console.log(data)
    })
}